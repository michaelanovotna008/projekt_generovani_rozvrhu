# Schedule Generator Project

## Launch
<p>The programme itself is located in the file "schedule_generator" and it requires the rest of the modules and the input file in the same directory.</p>

## Loading and Data Processing
<p>The programme loads input data (csv files in the "input" directory, which contain the data of availability of students and lectors).</p>
<p>The data is processed and the ideal variant of schedules for both of the lectors is evaluated.</p>

## Output
<p>The outcome is printed to a html file and also to CLI.</p>
<p>CLI shows also eventual problems (e.g. when a student is impossible to place to a course)</p>